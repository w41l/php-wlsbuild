#!/bin/sh

# Slackware build script for imagick php module
# Originally written by sl@not-only-pixel.de

# Copyright 2012-2016 Heinz Wiesinger, Amsterdam, The Netherlands
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# ChangeLog:
# 201912: Upgraded for version 3.4.4 - walecha99@gmail.com

SRCNAM=imagick
VERSION=${VERSION:-3.7.0}
PHPVER=$(php-config --version | cut -d . -f 1,2 | tr -d .)
PRGNAM=php${PHPVER}-$SRCNAM
SBNAM=php-$SRCNAM
BUILD=${BUILD:-1}
TAG=${TAG:-_SBo}
PKGTYPE=${PKGTYPE:-tlz}

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i686 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$PRGNAM-$VERSION-$ARCH-${BUILD}${TAG}.$PKGTYPE"
  exit 0
fi

CWD=$(pwd)
TMP=${TMP:-/tmp/SBo}
PKG=$TMP/package-$PRGNAM
OUTPUT=${OUTPUT:-/tmp}

if [ "$ARCH" = "i386" ]; then
  SLKCFLAGS="-O2 -march=i386 -mcpu=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=pentium4 -mtune=generic"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -march=x86-64 -mtune=generic -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

set -e

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP
rm -rf $SRCNAM-$VERSION
rm -f package.xml
tar xvf $CWD/$SRCNAM-$VERSION.tgz
cd $SRCNAM-$VERSION
chown -R root:root .
find -L . \
 \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 -o -perm 511 \) \
 -exec chmod 755 {} \; -o \
 \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
 -exec chmod 644 {} \;

PHP_CONFIG=/usr/bin/php-config

/usr/bin/phpize

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --docdir=/usr/doc/$PRGNAM-$VERSION \
  --with-php-config=$PHP_CONFIG \
  --build=$ARCH-slackware-linux

make

EXTENSION_DIR="$PKG/$($PHP_CONFIG --extension-dir)"
INCLUDE_DIR="$PKG/$($PHP_CONFIG --include-dir)"
mkdir -p $EXTENSION_DIR $INCLUDE_DIR $PKG/etc/php.d

make \
  EXTENSION_DIR=$EXTENSION_DIR \
  phpincludedir=$INCLUDE_DIR \
  DESTDIR=$PKG \
  install

mkdir -p $PKG/usr/lib$LIBDIRSUFFIX/php/.pkgxml
install -m 644 $TMP/package.xml $PKG/usr/lib$LIBDIRSUFFIX/php/.pkgxml/$SRCNAM.xml

install -m 644 $CWD/imagick.ini $PKG/etc/php.d/$SRCNAM.ini.new

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null || true

mkdir -p $PKG/usr/doc/$PRGNAM-$VERSION/examples
cp ChangeLog CREDITS LICENSE \
  $PKG/usr/doc/$PRGNAM-$VERSION/
cp -a examples/*.php \
  $PKG/usr/doc/$PRGNAM-$VERSION/examples/
cat $CWD/$SBNAM.SlackBuild > $PKG/usr/doc/$PRGNAM-$VERSION/$SBNAM.SlackBuild

mkdir -p $PKG/install
sed "s/${SBNAM}:/${PRGNAM}:/g" $CWD/slack-desc >$PKG/install/slack-desc
sed \
  -e "s|@LIBDIRSUFFIX@|$LIBDIRSUFFIX|g" \
  -e "s|@SRCNAM@|$SRCNAM|g" \
  $CWD/doinst.sh >$PKG/install/doinst.sh

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE
