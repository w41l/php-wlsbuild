#!/bin/bash

# Build and package mod_php on Slackware.
# by:  David Cantrell <david@slackware.com>
# Modified for PHP 4-5 by volkerdi@slackware.com
# Copyright 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2015, 2017, 2019, 2020  Patrick Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

cd $(dirname $0) ; CWD=$(pwd)

PKGNAM=php81
VERSION=${VERSION:-8.1.30}
ALPINE=${ALPINE:-2.26}
BUILD=${BUILD:-1}
TAG=${TAG:-"_wls"}
PKGTYPE=${PKGTYPE:-"txz"}

PHP_CONFIG_DIR="etc/$PKGNAM"

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

# If the variable PRINT_PACKAGE_NAME is set, then this script will report what
# the name of the created package would be, and then exit. This information
# could be useful to other scripts.
if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$PKGNAM-$VERSION-$ARCH-${BUILD}${TAG}.$PKGTYPE"
  exit 0
fi

if [ "$ARCH" = "i386" ]; then
  SLKCFLAGS="-O2 -march=i386 -mcpu=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i486" ]; then
  SLKCFLAGS="-O2 -march=i486 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "i686" ]; then
  SLKCFLAGS="-O2 -march=pentium4 -mtune=generic"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -march=x86-64 -mtune=generic -fPIC"
  LIBDIRSUFFIX="64"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

DOINST644="install -o ${OWNER:-root} -g ${GROUP:-root} -m 0644"
DOINST755="install -o ${OWNER:-root} -g ${GROUP:-root} -m 0755"

NUMJOBS=${NUMJOBS:-" -j$(expr $(nproc) + 1) "}
TMP=${TMP:-"/tmp/wlsbuild"}
PKG="$TMP/package-php"
OUTPUT=${OUTPUT:-"/tmp"}

rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT
cd $TMP

IMAPLIBDIR=/usr/local/lib${LIBDIRSUFFIX}/c-client
if [ -f $IMAPLIBDIR/lib${LIBDIRSUFFIX}/c-client.a ]; then
  # Look for Kerberos on the machine:
  if pkg-config --exists --print-errors krb5; then
    # Look for Kerberos in any precompiled c-client.a:
    if grep -q krb5_ $IMAPLIBDIR/lib${LIBDIRSUFFIX}/c-client.a 2> /dev/null ; then
      # Set Kerberos build option:
      KRB5_OPTION="--with-kerberos"
      echo "Using IMAP library with kerberos support:"
      ls -l $IMAPLIBDIR/lib${LIBDIRSUFFIX}/c-client.a
    else
      # Rebuild the c-client library if it does not contains Kerberos support:
      rm -rf $IMAPLIBDIR
      echo "Kerberos is installed. Rebuilding existing IMAP library with kerberos support"
    fi
  else
    # Look for Kerberos in any precompiled c-client.a:
    if grep -q krb5_ $IMAPLIBDIR/lib${LIBDIRSUFFIX}/c-client.a 2> /dev/null ; then
      # Remove the c-client library if it contains Kerberos support:
      rm -rf $IMAPLIBDIR
      echo "Kerberos is not installed. Rebuilding existing IMAP library without kerberos support"
    else
      # Remove kerberos support
      unset KRB5_OPTION
      echo "Using IMAP library without kerberos support:"
      ls -l $IMAPLIBDIR/lib${LIBDIRSUFFIX}/c-client.a
    fi
  fi
fi

if [ ! -f $IMAPLIBDIR/lib${LIBDIRSUFFIX}/c-client.a ]; then
  # we need to compile alpine to get c-client.a for IMAP support:
  echo "Building IMAP library using alpine"
  ( cd $CWD/../alpine ; VERSION=${ALPINE} ; NUMJOBS=" -j1 " sh ./alpine.SlackBuild || exit 1 ) || exit 1
  ( cd $TMP/alpine-${ALPINE}/imap/c-client
    strip -g c-client.a
    mkdir -p $IMAPLIBDIR/lib${LIBDIRSUFFIX}
    $DOINST644 c-client.a $IMAPLIBDIR/lib${LIBDIRSUFFIX}
    mkdir -p $IMAPLIBDIR/include
    for x in *.h; do
      if [ -L $x ]; then
        $DOINST644 $(readlink $x) $IMAPLIBDIR/include/$x
      else
        $DOINST644 $x $IMAPLIBDIR/include/$x
      fi
    done
  )
  if [ -f $IMAPLIBDIR/lib${LIBDIRSUFFIX}/c-client.a ]; then
    # Look for Kerberos in any precompiled c-client.a:
    if grep -q krb5_ $IMAPLIBDIR/lib${LIBDIRSUFFIX}/c-client.a 2> /dev/null ; then
      echo "Using IMAP library with kerberos support:"
      # Set Kerberos build option:
      KRB5_OPTION="--with-kerberos"
    else
      echo "Using IMAP library without kerberos support:"
      # Remove kerberos support
      unset KRB5_OPTION
    fi
    ls -l $IMAPLIBDIR/lib${LIBDIRSUFFIX}/c-client.a
  fi
fi

if pkg-config --exists odbc 2>/dev/null; then
  UNSUPPORTED=1
  ODBC_DRIVER="--with-unixODBC=shared,/usr --with-pdo-odbc=shared,unixODBC,/usr"
  echo "WARNING: Using upstream unsupported ODBC driver: unixODBC"
  export ODBC_CFLAGS=$(pkg-config --cflags odbc)
  export ODBC_LIBS=$(pkg-config --libs odbc)
else
  ODBC_DRIVER="--with-iodbc=shared,/usr --with-pdo-odbc=shared,iODBC,/usr"
fi

mkdir -p $PKG/$PHP_CONFIG_DIR/{conf,php-fpm}.d

SAPI_BUILD=${SAPI_BUILD:-"embed"}
if [ "$SAPI_BUILD" == "embed" ]; then
  UNSUPPORTED=1
  SAPI_OPTION="--enable-embed"
  echo "Using upstream unsupported SAPI type: embed"
else
  # A trick from DaMouse to enable building php into $PKG.
  # We'll remove this later on.
  mkdir -p $PKG/etc/httpd
  $DOINST644 /etc/httpd/original/httpd.conf $PKG/etc/httpd/httpd.conf
  if [ ! -e /etc/httpd/original/httpd.conf ]; then
    echo "FATAL:  no /etc/httpd/original/httpd.conf found."
    exit 1
  fi
  SAPI_OPTION="--with-apxs2=/usr/bin/apxs"
fi

if [ $UNSUPPORTED -eq 1 ]; then
  # Add delay before doing unsupported build
  echo "Do you accept this configuration (Press CTRL+C to cancel build)?"
  n=1
  until [ $n -gt 10 ]; do
    echo -n "."
    sleep 1
    ((n++))
  done
  echo "continue building php-$VERSION"
fi

cd $TMP
rm -rf php-$VERSION
tar xvf $CWD/php-$VERSION.tar.xz || exit 1
cd php-$VERSION || exit 1

# Fixup perms/owners:
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \+ -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \+

find . -name "*.h" -exec chmod 644 {} \+

if [ "$ARCH" = "s390" ]; then
  patch -Np1 <$CWD/php.configure.s390.diff || exit
fi

# Patch ini files:
patch -Np1 <$CWD/php.ini-development.diff || exit 1
patch -Np1 php.ini-production <$CWD/php.ini-development.diff || exit 1
patch -Np1 <$CWD/php-fpm.conf.diff || exit 1

# Fix for imap API change:
patch -Np1 <$CWD/php.imap.api.diff || exit 1

# Fix build with libxml2 2.12
# Info: https://github.com/php/php-src/issues/12965#issuecomment-1859159232
if [ $(pkg-config --modversion --print-errors libxml-2.0 | tr -d '.') -gt 2119 ]; then
  patch -Np1 <$CWD/061058a9b1bbd90d27d97d79aebcf2b5029767b0.patch || exit 1
  patch -Np1 <$CWD/0a39890c967aa57225bb6bdf4821aff7a3a3c082.patch || exit 1
fi

# cleanup:
find . -name "*.orig" -delete

# Install the build folder into /usr/lib$LIBDIRSUFFIX/php/build
# and adapt phpize accordingly:
sed -i "s|build$|php/build|" scripts/Makefile.frag
sed -i "s|build\"$|php/build\"|" scripts/phpize.in

# NOTE: Added -DU_USING_ICU_NAMESPACE=1 to CXXFLAGS, which should be a temporary
# requirement. See the link below:
# http://site.icu-project.org/download/61#TOC-Migration-Issues

# -DU_DEFINE_FALSE_AND_TRUE=1 since recent icu4c no longer defines these otherwise.

# Generic "kitchen sink" configure function, with as many things as possible (and
# maybe then some ;-) compiled as shared extensions:
EXTENSION_DIR=/usr/lib${LIBDIRSUFFIX}/php/extensions \
CFLAGS="$SLKCFLAGS -DU_DEFINE_FALSE_AND_TRUE=1 " \
CXXFLAGS="$SLKCFLAGS -DU_USING_ICU_NAMESPACE=1 -DU_DEFINE_FALSE_AND_TRUE=1 " \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --with-libdir=lib${LIBDIRSUFFIX} \
  --localstatedir=/var \
  --sysconfdir=/etc \
  --datarootdir=/usr/share \
  --datadir=/usr/share \
  --infodir=/usr/info \
  --mandir=/usr/man \
  $SAPI_OPTION \
  --enable-zts \
  --enable-fpm \
  --with-fpm-user=apache \
  --with-fpm-group=apache \
  --enable-maintainer-zts \
  --enable-pcntl \
  --enable-mbregex \
  --enable-tokenizer=shared \
  --with-config-file-scan-dir=/$PHP_CONFIG_DIR/conf.d \
  --with-config-file-path=/$PHP_CONFIG_DIR \
  --with-layout=PHP \
  --disable-sigchild \
  --with-libxml \
  --with-xmlrpc=shared \
  --with-expat \
  --enable-simplexml \
  --enable-xmlreader=shared \
  --enable-dom=shared \
  --enable-filter \
  --disable-debug \
  --with-openssl=shared \
  $KRB5_OPTION \
  --with-external-pcre \
  --with-zlib=shared,/usr \
  --enable-bcmath=shared \
  --with-bz2=shared,/usr \
  --enable-calendar=shared \
  --enable-ctype=shared \
  --with-curl=shared \
  --enable-dba=shared \
  --with-gdbm=/usr \
  --with-db4=/usr \
  --enable-exif=shared \
  --enable-ftp=shared \
  --enable-gd=shared \
  --with-external-gd \
  --with-jpeg \
  --with-xpm \
  --with-gettext=shared,/usr \
  --with-gmp=shared,/usr \
  --with-iconv=shared \
  --with-imap-ssl=/usr \
  --with-imap=$IMAPLIBDIR \
  --with-ldap=shared \
  --enable-mbstring=shared \
  --enable-mysqlnd=shared \
  --with-mysqli=shared,mysqlnd \
  --with-mysql-sock=/var/run/mysql/mysql.sock \
  $ODBC_DRIVER \
  --enable-pdo=shared \
  --with-pdo-mysql=shared,mysqlnd \
  --with-pdo-sqlite=shared,/usr \
  --with-pspell=shared,/usr \
  --with-enchant=shared,/usr \
  --enable-shmop=shared \
  --with-snmp=shared,/usr \
  --enable-soap=shared \
  --enable-sockets \
  --with-sqlite3=shared \
  --enable-sysvmsg \
  --enable-sysvsem \
  --enable-sysvshm \
  --with-xsl=shared,/usr \
  --with-zip=shared \
  --with-tsrm-pthreads \
  --enable-intl=shared \
  --enable-opcache \
  --enable-shared=yes \
  --enable-static=no \
  --with-gnu-ld \
  --with-pic \
  --enable-phpdbg \
  --with-sodium \
  --with-password-argon2 \
  --without-readline \
  --with-libedit \
  --with-pear \
  --with-tidy=shared \
  --build=$ARCH-slackware-linux || exit 1

# I am told this option is worse than nothing.  :-)
#  --enable-safe-mode
#
# I would recommend *against* and will take no responbility for turning on
# "safe" mode.

make $NUMJOBS || make -j1 -v || exit 1
make install INSTALL_ROOT=$PKG || exit 1

# Don't include the c-client library in php-config output:
sed -i "s| -L/usr/local/lib${LIBDIRSUFFIX}/c-client/lib${LIBDIRSUFFIX}||g" $PKG/usr/bin/php-config
sed -i "s|  -lc-client||g" $PKG/usr/bin/php-config

mkdir -p $PKG/etc/rc.d
$DOINST644 sapi/fpm/init.d.php-fpm $PKG/etc/rc.d/rc.php-fpm.new

# PHP (used to) install Pear with some strange permissions.
chmod 755 $PKG/usr/bin/pear

# PHP sometimes puts junk in the root directory:
( cd $PKG
  rm -rf .channels .depdb .depdblock .filemap .lock .registry
)

# We do not package static extension libraries:
rm -f $PKG/usr/lib${LIBDIRSUFFIX}/php/extensions/*.a

# Fix $PKG/usr/lib/php perms:
( cd $PKG/usr/lib${LIBDIRSUFFIX}/php
  find . \
   \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
   -exec chmod 755 {} \+ -o \
   \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
   -exec chmod 644 {} \+
)

mkdir -p $PKG/usr/doc/php-$VERSION
cp -a \
  CODING_STANDARDS* CONTRIBUTING* EXTENSIONS* LICENSE* NEWS* README* UPGRADING* \
  $PKG/usr/doc/php-$VERSION

if [ ! "$SAPI_BUILD" = "embed" ] || [ "$SAPI_BUILD" == "apache" ]; then
  mkdir -p $PKG/etc/httpd
  sed -e "s#lib/httpd#lib${LIBDIRSUFFIX}/httpd#" $CWD/mod_php.conf.example >$PKG/etc/httpd/mod_php.conf.new
  chmod 644 $PKG/etc/httpd/*
  chown root:root $PKG/etc/httpd/*
  # This can go now.
  rm -f $PKG/etc/httpd/httpd*
fi

$DOINST644 php.ini-development $PKG/$PHP_CONFIG_DIR/php.ini-development
$DOINST644 php.ini-production $PKG/$PHP_CONFIG_DIR/php.ini-production
chmod 755 $PKG/$PHP_CONFIG_DIR $PKG/$PHP_CONFIG_DIR/php-fpm.d
chown root:root $PKG/etc/*

# Session directory for PHP:
mkdir -p $PKG/var/lib/php
chmod 770 $PKG/var/lib/php
chown root:apache $PKG/var/lib/php

# Strip ELF objects.
find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

gzip -9 $PKG/usr/man/man?/*.?

mkdir -p $PKG/install
$DOINST644 $CWD/slack-desc $PKG/install/slack-desc
sed "s/@PHP_CONFIG_DIR@/$PHP_CONFIG_DIR/g" $CWD/doinst.sh >$PKG/install/doinst.sh
if [ ! "$SAPI_BUILD" = "embed" ] || [ "$SAPI_BUILD" == "apache" ]; then
  echo "config etc/httpd/mod_php.conf.new" >>$PKG/install/doinst.sh
  echo "config etc/httpd/mod_php.conf.new" >>$PKG/install/doinst.sh
fi

cd $PKG
/sbin/makepkg -l y -c n $OUTPUT/$PKGNAM-$VERSION-$ARCH-${BUILD}${TAG}.$PKGTYPE
